import { Component, NgZone } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

import * as identicon from 'identicon.js';

import { Message } from './message';
import { Tracker } from './tracker';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent
{
  constructor(private _ngZone: NgZone, private modalService: NgbModal, private _DomSanitizationService: DomSanitizer)
  {
    this.network = (<any>window).wispeer.net

    let onMetaChange: (metaData: any, trackerId: number) => void = (metaData, trackerId) =>
    {
      this._ngZone.run(() =>
      {
        this.parseMetaData(metaData, trackerId)
      })
    }

    this.network.setOnMetaChangeCB(onMetaChange)

    let onMessage: (message: string, trackerId: number) => void = (message, trackerId) =>
    {
      this._ngZone.run(() =>
      {
        let messageToAdd: Message = this.decodeMessage(message, trackerId);
        if (messageToAdd)
        {
          this.messages[this.messages.length] = messageToAdd
          this.sortMessage()
        }

      })
    }
    this.network.setMessageCB(onMessage)

  }

  sortMessage(): void
  {
    this.messages.sort((left, right) =>
    {
      if (left.date > right.date) return 1
      if (left.date < right.date) return -1
      return 0
    })
  }

  sendMessage(message?: Message): boolean
  {
    let messageStr: any
    let trackerList: Tracker[] = null
    if (!message)
    {
      messageStr = this.encodeMessage(this.currentMessage, this.currentAuthor, this.currentTitle, this.currentLink, this.currentTrackersId, this.currentFile)
    }
    else
    {
      messageStr = this.encodeMessageObject(message)
      trackerList = message.trackers
    }

    if (!trackerList)
    {
      let i: number = 0
      while (i < this.currentTrackersId.length)
      {
        this.sendMessageToTrackerId(messageStr, this.currentTrackersId[i])
        i++
      }
      //TODO: forbid message without tracker selected
      if (i === 0)
      {
        this.sendMessageToTrackerId(messageStr, 0)
      }
    }
    else
    {
      let i: number = 0
      while (i < trackerList.length)
      {
        this.sendMessageToTracker(messageStr, trackerList[i])
        i++
      }
      //TODO: forbid scream without tracker selected
      if (i === 0)
      {
        this.sendMessageToTrackerId(messageStr, 0)
      }
    }

    if (!message)
    {
      this.currentMessage = ""
      this.currentTitle = ""
      this.currentLink = ""
      this.currentFile = null
      return true
    }
    else
    {
      message.isScreamed = true
      return true
    }
  }

  sendMessageToTrackerId(message: string, trackerId: number): void
  {
    this.network.sendMessage(trackerId, message)
  }
  sendMessageToTracker(message: string, tracker: Tracker): void
  {
    this.sendMessageToTrackerId(message, this.getTrackerId(tracker))

  }

  decodeMessage(inputMessage: string, trackerId): Message
  {
    let messageParsed: any = inputMessage
    let tracker: Tracker = this.trackers[trackerId] ? this.trackers[trackerId] : null
    let res: Message =
      {
        message: "", author: "", title: "", link: "", date: new Date(), isScreamed: false, trackers: [], proposedTrackers: [], dissmissed: false, media: null
      }

    if (messageParsed.message)
    {
      res.message = messageParsed.message
    }


    if (messageParsed.author)
    {
      res.author = messageParsed.author
      this.generateIdenticon(res.author)
    }


    if (messageParsed.title)
    {
      res.title = messageParsed.title
    }


    if (messageParsed.link)
    {
      res.link = messageParsed.link
    }


    if (messageParsed.date)
    {
      res.date = messageParsed.date
    }

    if (messageParsed.dissmissed)
    {
      res.dissmissed = messageParsed.dissmissed
    }

    if (messageParsed.stream)
    {
      console.log("add Stream")
      res.media = messageParsed.stream
    }
    else
    {
      console.log("add pas stream")
    }

    if (messageParsed.trackers)
    {
      //      console.log(JSON.stringify(messageParsed.trackers))
      for (let i: number = 0; i < messageParsed.trackers.length; i++)
      {
        let t: Tracker = this.getTrackerWithURL(messageParsed.trackers[i].url, messageParsed.trackers[i].domain)
        if (t)
        {
          res.trackers[res.trackers.length] = t
          console.log("for message " + res.message + " add known tracker " + t.name)
        }
        else
        {
          console.log("for message " + res.message + " add unknown tracker " + messageParsed.trackers[i].name)
          res.proposedTrackers[res.proposedTrackers.length] = { "name": messageParsed.trackers[i].name, "url": messageParsed.trackers[i].url, "domain": messageParsed.trackers[i].domain, "status": messageParsed.trackers[i].status, "maxPeers": messageParsed.trackers[i].maxPeers, "nbPeers": messageParsed.trackers[i].nbPeers }
        }
      }
      // [{"name":"global en","url":"wss://torrero.fr/wispeer-tracker","domain":"wis-peer","status":"opened","maxPeers":50,"nbPeers":0},{"name":"global fr","url":"wss://torrero.fr/wispeer-tracker-fr","domain":"wis-peer","status":"opened","maxPeers":50,"nbPeers":0},{"name":"faux tracker","url":"wss://torrero.fr/ce-tracker-n-existe-pas","domain":"wis-peer","status":"unknown","maxPeers":50,"nbPeers":0}]
    }

    let i: number = 0
    let found: boolean = false
    while (i < this.messages.length && !found)
    {
      if (this.isMessagesEqual(this.messages[i], res))
      {
        found = true
      }
      else
      {
        i++
      }

    }

    if (found)
    {
      let k: number = 0
      while (res.trackers[k])
      {
        found = false
        let j: number = 0
        while (j < this.messages[i].trackers.length && !found)
        {
          if (res.trackers[k] === this.messages[i].trackers[j])
          {
            found = true
          }
          j++
        }
        if (!found)
        {
          this.messages[i].trackers[this.messages[i].trackers.length] = res.trackers[k]
        }
        k++
      }
      res = null
    }
    return res

  }

  private getTrackerWithURL(url: string, domain: string): Tracker
  {
    for (let i: number = 0; i < this.trackers.length; i++)
    {
      if (this.trackers[i].url === url && this.trackers[i].domain === domain)
      {
        return this.trackers[i]
      }
      else
      {
        console.log(this.trackers[i].url + "==!" + url + "&&" + this.trackers[i].domain + "==!" + domain)
      }
    }
    return null
  }

  private isMessagesEqual(message1: Message, message2: Message): boolean
  {
    return (message1.message === message2.message && message1.author === message2.author)
  }

  encodeMessageObject(message: Message): any
  {
    return message
    //return JSON.stringify(message)
  }

  encodeMessage(message: string, author?: string, title?: string, link?: string, trackers?: number[], file?: File): any
  {
    let res: any = {}
    res.message = message

    if (author)
    {
      res.author = author
    }
    else
    {
      res.author = ""
    }

    if (title)
    {
      res.title = title
    }
    else
    {
      res.title = ""
    }

    if (link)
    {
      res.link = link
    }
    else
    {
      res.link = ""
    }
    if (file)
    {
      console.log("add toTorrent")
      res.toTorrent = file
    }
    else
    {
      console.log("add pas toTorrent")
    }

    if (trackers)
    {
      res.trackers = []
      for (let i: number = 0; i < trackers.length; i++)
      {
        res.trackers[res.trackers.length] = this.trackers[trackers[i]]
      }
      console.log("trackers encode = " + JSON.stringify(res.trackers))
    }

    res.date = new Date()
    //console.log(JSON.stringify(res))
    //return JSON.stringify(res)
    return res
  }

  parseMetaData(metaData: any, trackerId: number): void
  {
    this.parseTrackerStatus(metaData, trackerId)
    this.parsePeers(metaData, trackerId)
  }

  parseTrackerStatus(metaData: any, trackerId: number): void
  {
    if (metaData.trackerStatus)
    {
      if (this.trackers.length <= trackerId)
      {
        this.updateTrackers()
      }
      this.trackers[trackerId].status = metaData.trackerStatus
    }
  }

  parsePeers(metaData: any, trackerId: number): void
  {
    if (metaData.nbPeers)
    {
      this.nbPeer = metaData.nbPeers
    }
    if (metaData.maxPeers)
    {
      this.maxPeers = metaData.maxPeers
    }
  }
  open(content)
  {
    let modal = this.modalService.open(content).result.then((result) =>
    {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) =>
      {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string
  {
    if (reason === ModalDismissReasons.ESC)
    {
      return 'by pressing ESC';
    }
    else if (reason === ModalDismissReasons.BACKDROP_CLICK)
    {
      return 'by clicking on a backdrop';
    }
    else
    {
      return `with: ${reason}`;
    }
  }

  updateTrackers(): void
  {
    let conf: any = this.network.getConf()

    for (let i: number = 0; i < conf.trackers.length; i++)
    {
      let t: any = conf.trackers[i]
      this.trackers[i] = { name: t.name, url: t.url, domain: t.domain, status: "unknow", maxPeers: t.maxPeers, nbPeers: 0 }
    }

  }

  addTracker(url: string, domain: string, name: string, maxPeers: number): void
  {
    let newTrackerId: number = this.network.addTracker(url, domain, name, maxPeers)
    //if everything went well, the new ID is also the next ID of GUI tracker list
    if (newTrackerId !== this.trackers.length)
    {
      console.log("Id problem detected!")
    }
    this._ngZone.run(() =>
    {
      let newTracker: Tracker = { name: name, url: url, domain: domain, status: "unknow", maxPeers: maxPeers, nbPeers: 0 }
      this.trackers[this.trackers.length] = newTracker
      console.log("add tracker " + newTracker.name)
      this.updateMessagesWithNewTracker(newTracker)
    })
  }

  updateMessagesWithNewTracker(newTracker: Tracker): void
  {

    for (let message of this.messages)
    {
      let trackerList: Tracker[] = message.proposedTrackers
      for (let i: number = 0; i < message.proposedTrackers.length; i++)
      {
        let t: Tracker = message.proposedTrackers[i]
        if (t.url === newTracker.url && t.domain === newTracker.domain)
        {
          message.proposedTrackers.splice(i, 1)
          message.trackers[message.trackers.length] = newTracker
          break
        }
      }
    }
  }

  addCurrentTracker(): void
  {
    this.addTracker(this.currentTrackerURL, this.currentTrackerDomain, this.currentTrackerName, 50)
  }

  getTrackerId(tracker: Tracker): number
  {
    let i: number = 0
    while (i < this.trackers.length)
    {
      if (this.trackers[i] === tracker)
      {
        return i
      }
      i++
    }
    return -1
  }

  isTrackerSelected(tracker: Tracker): boolean
  {
    let i: number = 0
    let id: number = this.getTrackerId(tracker)
    while (i < this.currentTrackersId.length)
    {
      if (this.currentTrackersId[i] === id)
      {
        return true
      }
      i++
    }
    return false
  }

  toggleTracker(tracker: Tracker): void
  {
    let i: number = 0
    let id: number = this.getTrackerId(tracker)
    while (i < this.currentTrackersId.length)
    {
      if (this.currentTrackersId[i] === id)
      {
        this.currentTrackersId.splice(i, 1)
        return
      }
      i++
    }
    this.currentTrackersId[this.currentTrackersId.length] = id
  }

  openSettingsForMessage(message: Message, content: any): void
  {
    this.currentTrackerList = message.trackers
    this.open(content)
  }

  isTrackerCurrentlySelected(tracker: Tracker): boolean
  {
    let i: number = 0
    while (i < this.currentTrackerList.length)
    {
      if (this.currentTrackerList[i] === tracker)
      {
        return true
      }
      i++
    }
    return false
  }


  toggleCurrentTracker(tracker: Tracker): void
  {
    let i: number = 0
    while (i < this.currentTrackerList.length)
    {
      if (this.currentTrackerList[i] === tracker)
      {
        this.currentTrackerList.splice(i, 1)
        return
      }
      i++
    }
    this.currentTrackerList[this.currentTrackerList.length] = tracker
  }

  setDissmissedMessage(message: Message, dissmissed: boolean): void
  {
    message.dissmissed = dissmissed
    this.network.setDissmissedOnMessage(message, dissmissed)
  }

  generateIdenticon(author: string): void
  {
    if (!this.authorImages[author] || !this.authorImages[author].img)
    {
      if (!this.authorImages[author])
      {
        this.authorImages[author] = {}
      }

      if (!this.authorImages[author].generating)
      {

        this.authorImages[author].generating = true

        // set up options
        var options = {
          background: [255, 255, 255, 0],         // transparent
          margin: 0,                              // 0% margin
          size: 32,                                // 32px square
          format: 'svg'                             // use SVG instead of PNG
        };

        this._ngZone.run(() =>
        {
          // create a base64 encoded SVG
          var data = new identicon(author, options).toString();
          this.authorImages[author].generating = false
          this.authorImages[author].img = 'data:image/svg+xml;base64,' + data
        })
      }
    }
  }

  updateMediaFileUploadForm(files: any): void
  {

    for (var i = 0, file; file = files[i]; i++)
    {
      this.currentFile = file
    }
  }

  addMediaToDOM(file: any, rootElement: any): string
  {
    if (document.getElementById(rootElement).innerHTML === "")
    {
      console.log("append !")
      file.appendTo(document.getElementById(rootElement), { autoplay: false })
      var tags = document.getElementById(rootElement).getElementsByTagName("*")
      for (var i = 0; i < tags.length; i++)
      {
        tags[i].classList.add("img-fluid")
      }
      return ""
    }
  }

  network: any
  messages: Message[] = []
  currentAuthor: string = ""
  currentTitle: string = ""
  currentMessage: string = ""
  currentLink: string = ""
  currentFile: File = null
  currentTrackersId: number[] = []

  trackers: Tracker[] = []
  nbPeer: number = 0
  maxPeers: number = 0
  currentTrackerURL: string = ""
  currentTrackerName: string = ""
  currentTrackerDomain: string = "wis-peer"

  currentTrackerList: Tracker[] = []

  trackerInfoShown: boolean = false

  authorImages: any = {}

  closeResult: string;



}
