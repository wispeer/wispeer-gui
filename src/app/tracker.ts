export class Tracker
{
  name: string
  url: string
  domain: string
  nbPeers: number = 0
  maxPeers: number
  status: string = "unknowk"
}
