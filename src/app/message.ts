import { Tracker } from './tracker';

export class Message
{
  message: string
  author: string
  title: string
  link: string
  date: Date
  isScreamed: boolean
  trackers: Tracker[]
  proposedTrackers: Tracker[]
  dissmissed: boolean
  media: any
}
