import { WisPeerGuiPage } from './app.po';

describe('wis-peer-gui App', () => {
  let page: WisPeerGuiPage;

  beforeEach(() => {
    page = new WisPeerGuiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
